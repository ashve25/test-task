<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
    Route::resource('roles','RoleController');
    Route::resource('products', 'ProductController');

    //Individual route access for each type of permission provided to the user roles
    Route::post('products/create', 'ProductController@store')->middleware('permission:product-create');
    Route::post('products/{id}', 'ProductController@update')->middleware('permission:product-edit');
    Route::get('products', 'ProductController@index')->middleware('permission:product-list');
});

Route::post('register', 'PassportController@register');

Route::post('login', 'PassportController@login');

Route::get('test', function (){
    return "test";
});